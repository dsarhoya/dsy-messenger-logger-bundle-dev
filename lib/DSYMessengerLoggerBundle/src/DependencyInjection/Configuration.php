<?php

namespace DSY\DSYMessengerLoggerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('dsy_messenger_logger');
        $rootNode = $treeBuilder->getRootNode();

        return $treeBuilder;
    }
}
