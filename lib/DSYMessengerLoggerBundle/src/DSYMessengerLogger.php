<?php

namespace DSY\DSYMessengerLoggerBundle;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use DSY\DSYMessengerLoggerBundle\Entity\LoggerMessage;
use DSY\DSYMessengerLoggerBundle\Repository\LoggerMessageRepository;

class DSYMessengerLogger
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function log($message = null, $context = null)
    {
        if (null == $message) {
            return 0;
        }
        $loggerInfo = new LoggerMessage();
        $loggerInfo->setDateTime(new DateTime('now'));
        $loggerInfo->setMessage($message);
        $loggerInfo->setContext($context);

        $this->entityManager->persist($loggerInfo);
        $this->entityManager->flush();
    }

    public function getLogs(array $options)
    {
        /** @var LoggerMessageRepository $repoLogger */
        $repoLogger = $this->entityManager->getRepository(Logger::class);

        $logs = $repoLogger->logger($options);

        return $logs;
    }
}
